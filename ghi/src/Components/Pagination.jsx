import React from 'react';

const Pagination = ({ currentPage, itemsPerPage, totalItems, onPageChange }) => {
    const totalPages = Math.ceil(totalItems / itemsPerPage);
    const pageNumbers = [];

    for (let i = 1; i <= totalPages; i++) {
        pageNumbers.push(i);
    }

    return (
        <nav>
            <ul className="pagination">
                <li
                    className={`page-item${currentPage === 1 ? ' disabled' : ''}`}
                >
                    <button
                        onClick={() => onPageChange(currentPage - 1)}
                        className="page-link mx-1"
                    >
                        Previous
                    </button>
                </li>

                {pageNumbers.map((pageNumber) => (
                    <li key={pageNumber} className="page-item mx-1">
                        <button
                            onClick={() => onPageChange(pageNumber)}
                            className={`page-link${currentPage === pageNumber ? ' active' : ''
                                }`}
                        >
                            {pageNumber}
                        </button>
                    </li>
                ))}
                <li
                    className={`page-item${currentPage === totalPages ? ' disabled' : ''
                        }`}
                >
                    <button
                        onClick={() => onPageChange(currentPage + 1)}
                        className="page-link mx-1"
                    >
                        Next
                    </button>
                </li>
            </ul>
        </nav>
    );
};

export default Pagination;
