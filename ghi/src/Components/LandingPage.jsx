import { Link } from 'react-router-dom';
import '../Styles/Home.css';

export default function LandingPage() {
    return (
        <><div className="App">
            <h1 className="display-5 fw-bold">Play Select</h1>
            <div className="col-lg-6 mx-auto">
                <p className="lead mb-4">
                    Explore the endless world of video games!
                </p>
            </div>
            <div className="button-container">
                <Link to="/login" role="button" className="original-button" id="loginBtn" type="submit">
                    LOG-IN!
                </Link>
                <Link to="/signUp" role="button" className="original-button" id="signupBtn" type="submit">
                    SIGN-UP!
                </Link>
            </div>
        </div>
        </>
    );
}
