import React from "react";
import { Link } from "react-router-dom";
import Controller from "../images/Controller.png";
import batman from "../images/batman_avatar.png";
import gitlab from "../images/gitlab.svg";
import github from "../images/github.svg";
import linkedin from "../images/linkedin.svg";
import email from "../images/email.svg";
import '../Styles/Creator.css';
import pain from "../images/pain_1.jpg";


export default function CreatorPage() {
    return (
        <div className="mt-3">
            <h1>PlaySelect Creators</h1>
            <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 mt-4">
                {/* Brandon's Card */}
                <div className="col-sm-6 mb-3 mb-sm-0">
                    <div className="creator-card">
                        <img
                            className="card-img-top"
                            src={batman}
                            alt="Brandonsphoto"
                        />
                        <div className="card-body">
                            <h5 className="card-title">Brandon Ramos</h5>
                            <p className="card-title">Full-Stack Software Engineer</p>
                            <p className="card-text">
                                Hello im Brandon Ramos i am an experienced software engineer and law enforcement professional based in Denver, CO.
                                With a Master's in Public Administration from the University of Phoenix and a B.S. in Fire and Emergency Services Administration from Metropolitan State University,I have a diverse skill set.
                                As a technical expert, I am proficient in Python, JavaScript, SQL, HTML5, and CSS, with expertise in various frameworks like React, Redux, Django, and FastAPI.
                                I have worked hard to learn and develope  full-stack applications such as  task management and car dealership services,and this game favorites page.
                                showcasing my ability to design secure and efficient systems. In addition to my engineering accomplishments,
                                I have served as a dedicated decorated  Police Officer and Detective at the Denver Police Department since 2019.
                                I have a passion for technology and my dedication to public service make him a well-rounded professional with a unique blend of technical and interpersonal skills.
                                You can find more about me on LinkedIn, GitLab, GitHub or via Email! Feel free to reach out to me!
                            </p>
                            <p className="card-text">
                                <span className="font-weight-bold">Hobbies/Interests: </span>
                                <br />
                                - Games rather Video games or Board games im always down to play
                                <br />
                                - Spending time with Family and Friends
                                <br />
                                - Learning, you can always learn something new each day!
                            </p>
                            <div className="card-links">
                                <Link
                                    to="https://github.com/Bramos17"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Github icon"
                                        src={github}
                                    />
                                </Link>
                                <Link
                                    to="https://www.linkedin.com/in/brandon-ramos17"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Linkedin icon"
                                        src={linkedin}
                                    />
                                </Link>
                                <Link
                                    to="https://gitlab.com/Bramos17"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Gitlab icon"
                                        src={gitlab}
                                    />
                                </Link>
                                <Link to="mailto:brandon-ramos17@outlook.com">
                                    <img
                                        className="card-link"
                                        alt="Email icon"
                                        src={email}
                                    />
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Myron's Card */}
                <div className="col-sm-6 mb-3 mb-sm-0">
                    <div className="creator-card">
                        <img
                            className="card-img-top-m"
                            src={pain}
                            alt="myronphoto"
                        />
                        <div className="card-body">
                            <h5 className="card-title">Myron Atlas</h5>
                            <p className="card-title">Full-Stack Software Engineer</p>
                            <p className="card-text">I am a USMC veteran born and raised in Mississippi I've always had a interest in tech and decide to take a leap of faith and learn how to code and it is the greatest decision I've made!</p>
                            <p className="card-text">
                                <span className="font-weight-bold">Hobbies/Interests: </span>
                                <br />
                                - Playing videos/streaming or watching sports
                                <br />
                                - Hanging with family and playing sports/going to the gym
                                <br />
                                - Using my creativity and using coding to make my ideas come alive
                            </p>
                            <div className="card-links">
                                <Link
                                    to="https://github.com/myronatlas"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Github icon"
                                        src={github}
                                    />
                                </Link>
                                <Link
                                    to="https://www.linkedin.com/in/myron-reed-atlas-24486b201"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Linkedin icon"
                                        src={linkedin}
                                    />
                                </Link>
                                <Link
                                    to="https://gitlab.com/"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Gitlab icon"
                                        src={gitlab}
                                    />
                                </Link>
                                <Link to="mailto:atlasmyron1@gmail.com">
                                    <img
                                        className="card-link"
                                        alt="Email icon"
                                        src={email}
                                    />
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Valerie's Card */}
                <div className="col-sm-6 mb-3 mb-sm-0">
                    <div className="creator-card">
                        <img
                            className="card-img-top"
                            src={Controller}
                            alt="valeriephoto"
                        />
                        <div className="card-body">
                            <h5 className="card-title">Valerie Shamshyna</h5>
                            <p className="card-title">Full-Stack Software Engineer</p>
                            <p className="card-text">
                                <span className="font-weight-bold">Hobbies/Interests: </span>
                                <br />
                                - Dancing, running, biking, horseback riding, bouldering, backpacking, and basically anything adventurous and active :)
                                <br />
                                - Spending time in nature
                                <br />
                                - Hanging out with animals, especially dogs and horses
                            </p>
                            <div className="card-links">
                                <Link
                                    to="https://github.com/valeriesham"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Github icon"
                                        src={github}
                                    />
                                </Link>
                                <Link
                                    to="https://www.linkedin.com/in/valerie-shamshyna/"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Linkedin icon"
                                        src={linkedin}
                                    />
                                </Link>
                                <Link
                                    to="https://gitlab.com/valeriesham"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Gitlab icon"
                                        src={gitlab}
                                    />
                                </Link>
                                <Link to="mailto:valeriesham13@gmail.com">
                                    <img
                                        className="card-link"
                                        alt="Email icon"
                                        src={email}
                                    />
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>

                {/* Jason's Card */}
                <div className="col-sm-6 mb-3 mb-sm-0">
                    <div className="creator-card">
                        <img
                            className="card-img-top"
                            src={Controller}
                            alt=""
                        />
                        <div className="card-body">
                            <h5 className="card-title">Jason Leisure</h5>
                            <p className="card-title">Full-Stack Software Engineer</p>
                            {/* <p className="card-text">ABOUT YOU HERE</p>
                            <p className="card-text">
                                <span className="font-weight-bold">Hobbies/Interests: </span>
                                <br />
                                - 1
                                <br />
                                - 2
                                <br />
                                - 3
                            </p> */}
                            <div className="card-links">
                                <Link
                                    to="https://github.com/"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Github icon"
                                        src={github}
                                    />
                                </Link>
                                <Link
                                    to="https://www.linkedin.com/in/"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Linkedin icon"
                                        src={linkedin}
                                    />
                                </Link>
                                <Link
                                    to="https://gitlab.com/JasonLeisure"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <img
                                        className="card-link"
                                        alt="Gitlab icon"
                                        src={gitlab}
                                    />
                                </Link>
                                {/* <Link to="mailto:youremailhere">
                                    <img
                                        className="card-link"
                                        alt="Email icon"
                                        src={email}
                                    />
                                </Link> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
