import React from 'react';
import { useGetDeveloperQuery } from '../Store/apiSlice';
import { useSelector } from 'react-redux';
import '../Styles/Cards.css';


const DevelopersList = () => {
    const searchCriteria = useSelector((state) => state.search.value);
    const { data, isLoading } = useGetDeveloperQuery();

    if (isLoading) {
        return <div>Loading...</div>;
    }

    const filteredDevelopers = () => {
        if (searchCriteria) {
            const searchLowercase = searchCriteria.toLowerCase();
            return data.filter((developer) =>
                developer.name.toLowerCase().includes(searchLowercase)
            );
        } else {
            return data;
        }
    };

    return (
        <div className="mt-3">
            <h1>
                Developers
                <small id="seach-criteria"> {searchCriteria}</small>
            </h1>
            <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 mt-4">
                {filteredDevelopers().map((developer) => (
                    <div key={developer.id} className="col-sm-6 mb-3 mb-sm-0">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">{developer.name}</h5>
                                <p className="card-text">Game Count: {developer.games_count}</p>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default DevelopersList;
