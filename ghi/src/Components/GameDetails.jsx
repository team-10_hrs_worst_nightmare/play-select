import { useNavigate, useParams } from "react-router-dom";
import { useGetGameByIdQuery, useCreateFavoriteMutation } from "../Store/apiSlice";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import '../Styles/Cards.css';

const GameDetails = () => {
    const { game_id } = useParams();
    const { data, isLoading } = useGetGameByIdQuery(game_id);
    const [createFavoriteMutation] = useCreateFavoriteMutation();
    const navigate = useNavigate();

    console.log(data);

    if (isLoading) return <div>Loading Please Wait</div>;

    const name = data?.name;
    const platforms = data?.Platforms;

    const handleAddFavoriteWithToast = async () => {
        try {
            await createFavoriteMutation({
                game_id: game_id,
                game_name: name
            });

            toast.success("YOU ADDED A FAVORITE", {
                autoClose: 2000,
            });
            navigate("/favorites");;
        } catch (error) {
            console.error("Failed to add favorite:", error);
        }
    };

    return (
        <div className="card text-center">
            <div className="card-header">
                Featured
            </div>
            <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">Platforms:</h6>
                <ul className="list-group">
                    {platforms.map((platform) => (
                        <li key={platform.id} className="list-group-item">
                            {platform.name}
                        </li>
                    ))}
                </ul>
                <button className="btn btn-primary" onClick={handleAddFavoriteWithToast}>Favorite</button>
            </div>
            <ToastContainer />
        </div>
    );
};

export default GameDetails;
