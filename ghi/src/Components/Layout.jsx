import React from 'react';
import videoSource from '../Styles/video/background.mp4';

const Layout = ({ children }) => {
    return (
        <>
            <video
                style={{
                    position: "fixed",
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                    zIndex: "-1",
                    top: 0,
                    left: 0,
                    pointerEvents: "none",
                }}
                autoPlay
                loop
                muted
                src={videoSource}
                type="video/mp4"
            />
            {children}
        </>
    );
};

export default Layout;
