import React, { useState } from 'react';
import { useGetallGamesQuery } from '../Store/apiSlice';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import '../Styles/Cards.css';
import PaginationComponent from './Pagination';
const GameList = () => {
    const searchCriteria = useSelector((state) => state.search.value);
    const { data, isLoading } = useGetallGamesQuery();
    const [activePage, setActivePage] = useState(1);
    const itemsPerPage = 6;

    if (isLoading) return <div>Loading...</div>;
    const filteredGames = () => {
        if (searchCriteria) {
            const searchLowercase = searchCriteria.toLowerCase();
            return data.filter((game) =>
                game.name.toLowerCase().includes(searchLowercase)
            );
        } else {
            return data;
        }
    };

    const indexOfLastItem = activePage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = data?.slice(indexOfFirstItem, indexOfLastItem);
    const handlePageChange = (pageNumber) => {
        setActivePage(pageNumber);
    };
    return (
        <div className="mt-3">
            <h1>
                Games
                <small id="seach-criteria"> {searchCriteria}</small>
            </h1>
            <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 mt-4">
                {currentItems?.map((game) => (
                    <div key={game.id} className="col-sm-6 mb-3 mb-sm-0">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">
                                    <Link to={`/games/${game.id}`}>{game.name}</Link>
                                </h5>
                                <h6 className="card-subtitle mb-2 text-body-secondary">Platforms</h6>
                                <div className="row row-cols-2">
                                    {game.Platforms.map((platform) => (
                                        <div key={platform.id} className="col">
                                            <li className="list-group-item text-wrap">{platform.name}</li>
                                        </div>
                                    ))}

                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
            <PaginationComponent
                currentPage={activePage}
                itemsPerPage={itemsPerPage}
                totalItems={filteredGames()?.length}
                onPageChange={handlePageChange}
            />
        </div>
    );
};
export default GameList;
