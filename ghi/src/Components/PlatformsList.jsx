import React, { useState } from 'react';
import { useGetPlatformsQuery } from '../Store/apiSlice';
import { useSelector } from 'react-redux';
import Pagination from './Pagination';
import '../Styles/Cards.css';

const PlatformsList = () => {
    const searchCriteria = useSelector((state) => state.search.value);
    const { data, isLoading } = useGetPlatformsQuery();
    const itemsPerPage = 12;
    const [currentPage, setCurrentPage] = useState(1);

    if (isLoading) {
        return <div>Loading...</div>;
    }

    const filteredPlatforms = () => {
        if (searchCriteria) {
            const searchLowercase = searchCriteria.toLowerCase();
            return data.filter((platform) =>
                platform.name.toLowerCase().includes(searchLowercase)
            );
        } else {
            return data;
        }
    };

    const indexOfLastPlatform = currentPage * itemsPerPage;
    const indexOfFirstPlatform = indexOfLastPlatform - itemsPerPage;
    const currentPlatforms = filteredPlatforms().slice(
        indexOfFirstPlatform,
        indexOfLastPlatform
    );

    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    return (
        <div className="mt-3">
            <h1>
                Platforms
                <small id="seach-criteria"> {searchCriteria}</small>
            </h1>
            <div className="row row-cols-1 row-cols-md-2 row-cols-md-3 mt-4" id="platforms">
                {currentPlatforms.map((platform) => (
                    <div key={platform.id} className="col mb-4">
                        <div className="card mb-3">
                            <div className="card-body">
                                <h5 className="platform-title">{platform.name}</h5>
                                <p className="card-text">Game Count: {platform.games_count}</p>
                                <img src={platform.image_background} alt={platform.name} className="card-img-top" />
                            </div>
                        </div>
                    </div>
                ))}
            </div>
            <Pagination
                currentPage={currentPage}
                itemsPerPage={itemsPerPage}
                totalItems={filteredPlatforms().length}
                onPageChange={handlePageChange}
            />
        </div>
    );
};

export default PlatformsList;
