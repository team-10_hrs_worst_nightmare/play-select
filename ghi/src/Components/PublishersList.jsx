import React from 'react';
import { useGetPublisherQuery } from '../Store/apiSlice';
import { useSelector } from 'react-redux';
import '../Styles/Cards.css';


const PublishersList = () => {
    const searchCriteria = useSelector((state) => state.search.value);
    const { data, isLoading } = useGetPublisherQuery();

    if (isLoading) {
        return <div>Loading...</div>;
    }

    const filteredPublishers = () => {
        if (searchCriteria) {
            const searchLowercase = searchCriteria.toLowerCase();
            return data.filter((publisher) =>
                publisher.name.toLowerCase().includes(searchLowercase)
            );
        } else {
            return data;
        }
    };

    return (
        <div className="mt-3">
            <h1>
                Publishers
                <small id="seach-criteria"> {searchCriteria}</small>
            </h1>
            <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 mt-4">
                {filteredPublishers().map((publisher) => (
                    <div key={publisher.id} className="col-sm-6 mb-3 mb-sm-0">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title">{publisher.name}</h5>
                                <p className="card-text">Game Count: {publisher.games_count}</p>
                                <img src={publisher.image_background} alt={publisher.name} className="card-img-top" />
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default PublishersList;
