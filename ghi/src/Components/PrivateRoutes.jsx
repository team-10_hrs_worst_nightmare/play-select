import React from 'react';
import { Route, Navigate } from 'react-router-dom';

function PrivateRoute({ children, isLoggedIn, ...props }) {
    return (
        <Route {...props}>
            {isLoggedIn ? children : <Navigate to="/login" />}
        </Route>
    );
}
export default PrivateRoute;
