import Controller from '../images/Controller.png';
import Nav from './Nav';

const ErrorPage = () => {
    return (
        <div className="container">
            <Nav />
            <div className="mt-5">
                <h1>Page not found</h1>
                <img src={Controller} alt="" />
            </div>
        </div>
    )
}

export default ErrorPage;
