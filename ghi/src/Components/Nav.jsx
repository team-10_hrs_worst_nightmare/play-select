import "../Styles/Nav.css";
import Playslectnbg from "../images/PlaySelect.png";
import {  NavLink, useLocation } from "react-router-dom";
import Search from './Search'
import {  useLogoutMutation } from '../Store/apiSlice'

function Nav() {

    const [logout] = useLogoutMutation()
    const location = useLocation()
    const showSearchBar =
        location.pathname === '/games' ||
        location.pathname === '/platform-list' ||
        location.pathname === '/genre-list' ||
        location.pathname === '/Developer-list' ||
        location.pathname === '/Publisher-list'


    return (
        <>
            <nav className="navbar navbar-expand-lg" id="NavBar">
                <div className="container-fluid">
                    <NavLink className="navbar-brand" to="/">
                        <img className="navbar-logo" src={Playslectnbg} alt="Playslectnbg" />
                    </NavLink>

                    <button
                        className="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon" id="navbar menu">
                            Menu
                        </span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item dropdown">
                                <NavLink
                                    className="nav-link dropdown-toggle"
                                    id="navbarGamesDropdown"
                                    role="button"
                                    data-bs-toggle="dropdown"
                                    aria-expanded="false"
                                >
                                    Games
                                </NavLink>
                                <ul className="dropdown-menu" aria-labelledby="navbarGamesDropdown">
                                    <li className="dropdown-item">
                                        <NavLink to={'/games'} className={'nav-link'}>
                                            Games
                                        </NavLink>
                                    </li>
                                    <li className="dropdown-item">
                                        <NavLink to={'/platform-list'} className={'nav-link'}>
                                            Platform
                                        </NavLink>
                                    </li>
                                    <li className="dropdown-item">
                                        <NavLink to={'/genre-list'} className={'nav-link'}>
                                            Genre
                                        </NavLink>
                                    </li>
                                    <li className="dropdown-item">
                                        <NavLink to={'/Developer-list'} className={'nav-link'}>
                                            Developer
                                        </NavLink>
                                    </li>
                                    <li className="dropdown-item">
                                        <NavLink to={'/Publisher-list'} className={'nav-link'}>
                                            Publisher
                                        </NavLink>
                                    </li>
                                </ul>
                            </li>
                            <li className="nav-item dropdown">
                                <NavLink
                                    className="nav-link dropdown-toggle"
                                    id="navbarAccountsDropdown"
                                    role="button"
                                    data-bs-toggle="dropdown"
                                    aria-expanded="false"
                                >
                                    Accounts
                                </NavLink>
                                <ul className="dropdown-menu" aria-labelledby="navbarAccountsDropdown">
                                    <li className="dropdown-item">
                                        <NavLink to={'/profile'} className={'nav-link'}>
                                            Profile
                                        </NavLink>
                                    </li>
                                    <li className="dropdown-item">
                                        <NavLink to={'/favorites'} className={'nav-link'}>
                                            Favorites
                                        </NavLink>
                                    </li>
                                    <li className="dropdown-item">
                                        <div className="dropdown-item btn btn-primary" id="logout" onClick={logout}>
                                            Log Out
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        {showSearchBar && <Search />}
                    </div>
                </div>
            </nav> </>
    );
}

export default Nav;
