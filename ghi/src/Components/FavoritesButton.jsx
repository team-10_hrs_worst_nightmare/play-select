import {
    useGetFavoritesQuery,
    useDeleteFavoriteMutation,
    useCreateFavoriteMutation
} from "../Store/apiSlice";
import { useEffect, useState } from 'react';

const FavoritesButton = (props) => {
    const [favorite, setFavorite] = useState(null);
    const [deleteFavorite] = useDeleteFavoriteMutation();
    const [createFavorite] = useCreateFavoriteMutation();
    const { data: favorites } = useGetFavoritesQuery();

    useEffect(() => {
        if (favorites) {
            setFavorite(favorites.find(f => f.game_id === props.id) || null);
        }
    }, [favorites]);

    return (
        <>
            {!favorite && <button
                className="btn btn-success"
                onClick={() => createFavorite({ game_id: props.id })}
            >
                Favorite
            </button>}
            {favorite && <button
                className="btn btn-danger"
                onClick={() => deleteFavorite(favorite.id)}
            >
                Unfavorite
            </button>}
        </>
    )
}

export default FavoritesButton;
