import React, { useEffect, useState } from "react";
import { useGetFavoritesQuery, useGetGameByIdQuery } from "../Store/apiSlice";

const FavoritesGame = () => {
    const { data, isLoading, isError, error } = useGetFavoritesQuery();
    const [gameDetails, setGameDetails] = useState({});

    const getGameDetails = useGetGameByIdQuery();

    useEffect(() => {
        console.log("data:", data);
        if (data && data.favorites && data.favorites.length > 0) {
            data.favorites.forEach((favorite) => {

                getGameDetails.fetch(favorite.game_id).unwrap().then((result) => {

                    setGameDetails((prevGameDetails) => ({
                        ...prevGameDetails,
                        [favorite.game_id]: result,
                    }));
                });
            });
        }
    }, [data]);

    if (isLoading) return <div>Loading...</div>;

    if (isError) return <div>Error: {error.message}</div>;

    if (!data || !data.favorites || data.favorites.length === 0) {
        return <div>No favorite games found.</div>;
    }

    const favoritesArray = data.favorites;
    return (
        <div>
            <h2>My Favorite Games</h2>
            {favoritesArray.map((favorite) => (
                <div key={favorite.id}>
                    <h3>Game ID: {favorite.game_id}</h3>
                    {gameDetails[favorite.game_id] && (
                        <h4>Name: {gameDetails[favorite.game_id].name}</h4>
                    )}
                </div>
            ))}
        </div>
    );
};

export default FavoritesGame;
