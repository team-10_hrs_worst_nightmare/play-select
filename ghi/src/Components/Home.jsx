import React from 'react';
import '../Styles/Home.css';


function Home() {
    return (
        <div className="main-container">
            <div className="main-content">
                <h1 className="main-heading">
                    <span className="rotate-animation">P</span>
                    <span className="rotate-animation">l</span>
                    <span className="rotate-animation">a</span>
                    <span className="rotate-animation">y</span>
                    <span className="rotate-animation"> </span>
                    <span className="rotate-animation">S</span>
                    <span className="rotate-animation">e</span>
                    <span className="rotate-animation">l</span>
                    <span className="rotate-animation">e</span>
                    <span className="rotate-animation">c</span>
                    <span className="rotate-animation">t</span>
                </h1>
                <p className="slogan">Explore the endless world of video games!</p>
            </div>
        </div>
    );
}

export default Home;
