import React from "react";
import { useGetAccountQuery, useGetFavoritesQuery, useDeleteFavoriteMutation } from "../Store/apiSlice";

const Profile = () => {
    const { data: favorites, isLoading: isFavoritesLoading } = useGetFavoritesQuery();
    const { data: account, isLoading: isAccountLoading } = useGetAccountQuery();
    const [deleteFavoriteMutation, { refetch }] = useDeleteFavoriteMutation();

    console.log(account);

    const handleDeleteFavorite = async (favoriteId) => {
        try {
            await deleteFavoriteMutation({
                favoriteId: favoriteId
            });

            refetch();
        } catch (error) {
            console.error('Failed to remove favorite:', error);
        }
    };

    const GameCard = ({ name, platforms }) => {
        return (
            <div className="card mb-3" style={{ width: "16rem", marginRight: "15px" }}>
                <div className="card-body">
                    <h5 className="card-title">{name}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">Platforms:</h6>
                    <ul className="list-group">
                        {platforms.map((platform) => (
                            <li key={platform.id} className="list-group-item">
                                {platform.name}
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        );
    };

    if (isFavoritesLoading || isAccountLoading) {
        return <div>Loading...</div>;
    }

    return (
        <div className="container-fluid py-5 mx-auto">
            <div className="card py-4 px-4">
                <div className="row justify-content-start px-3">
                    <div className="image-bg mr-3">
                        <img className="user-img fit-image" src="https://" alt="User" />
                    </div>
                    <div className="text-left">
                        <h2>{account?.username}</h2>
                        <h6>{account?.first_name} - {account?.last_name} - {account?.email}</h6>
                        <button className="btn btn-primary">Edit Profile</button>
                    </div>
                </div>
                <div className="line"></div>
                <div id="carouselExample" className="carousel slide" data-bs-ride="carousel">
                    <div className="carousel-inner">
                        {Array.isArray(favorites) &&
                            favorites.map((favorite, index) => (
                                <div key={favorite.id} className={`carousel-item ${index === 0 ? 'active' : ''}`}>
                                    <GameCard name={favorite.game.title} platforms={favorite.game.platforms} />
                                    <button onClick={() => handleDeleteFavorite(favorite.id)}>Remove</button>
                                </div>
                            ))}
                    </div>
                    <button className="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
        </div>
    );
};

export default Profile;
