import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useLoginMutation } from "../Store/apiSlice";
import {Link} from "react-router-dom";
import "../Styles/Forms.css"

export default function LoginForm() {
    const navigate = useNavigate();
    const [login, loginResult] = useLoginMutation();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [ setErrorMessage] = useState('')

    useEffect(() => {
        if (loginResult.error) {
            if (loginResult.error.status === 401) {
                setErrorMessage(loginResult.error.data.detail);
            }
        }
        if (loginResult.isSuccess) navigate('/');
    }, [loginResult, navigate]);

    const handleSubmit = (e) => {
        e.preventDefault();
        login({username, password})
    }


    return (
        <div className="row">
            <div className="col-md-6 offset-md-3">
                <h1>Login</h1>
                <form onSubmit={handleSubmit} id="create-account-form" className="form">
                    <div className="mb-3">
                        <label htmlFor="Login__username" className="form-label">Username</label>
                        <input
                            type="text"
                            className="form-control"
                            id="Login__username"
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="Login__password" className="form-label">Password</label>
                        <input
                            type="password"
                            className="form-control"
                            id="Login__password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                    </div>
                    <div className="text-sm text-gray-500 mb-5">
                        Don't have an account?{" "}
                        <Link
                            className="SignInLink"
                            to="/signup"
                            onClick={(e) => {
                                e.preventDefault();
                                navigate("/signup");
                            }}
                        >
                            Sign Up here
                        </Link>
                    </div>
                    <button type="submit" className="original-button">LOGIN</button>

                </form>
            </div>
        </div>
    )
}
