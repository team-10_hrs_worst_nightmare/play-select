import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const gameApi = createApi({
    reducerPath: 'gameApi',
    baseQuery: fetchBaseQuery({
        baseUrl: process.env.REACT_APP_API_HOST,
    }),
    endpoints: (builder) => ({
        getallGames: builder.query({
            query: () => ({
                url: `/games`,
                credentials: 'include',
            }),
        }),
        GetGameById: builder.query({
            query: (game_id) => ({
                url: `/games/${game_id}`,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }),
        }),
        getPlatforms: builder.query({
            query: () => ({
                url: `/platforms`,
                credentials: 'include',
            }),
        }),
        getGenre: builder.query({
            query: () => ({
                url: `/genres`,
                credentials: 'include',
            }),
        }),
        getDeveloper: builder.query({
            query: () => ({
                url: `/developers`,
                credentials: 'include',
            }),
        }),
        getPublisher: builder.query({
            query: () => ({
                url: `/publishers`,
                credentials: 'include',
            }),
        }),
        getFavorites: builder.query({
            query: () => ({
                url: '/favorites/mine',
                credentials: 'include',
            }),
            providesTags: ['Favorites'],
        }),
        createFavorite: builder.mutation({
            query: (body) => ({
                url: '/favorites',
                body,
                method: 'POST',
                credentials: 'include',
            }),
            invalidatesTags: ['Favorites'],
        }),
        deleteFavorite: builder.mutation({
            query: (favorite_id) => ({
                url: `/favorites/${favorite_id}`,
                method: 'DELETE',
                credentials: 'include',
            }),
            invalidatesTags: ['Favorites'],
        }),
        getAccount: builder.query({
            query: () => ({
                url: `/token`,
                credentials: 'include',
            }),
            transformResponse: (response) => {
                return response ? response.account : null
            },
            providesTags: ['Account'],
        }),
        logout: builder.mutation({
            query: () => ({
                url: '/token',
                method: 'DELETE',
                credentials: 'include',
            }),
            invalidatesTags: ['Account'],
        }),


        signup: builder.mutation({
            query: (body) => ({
                url: `/accounts`,
                method: 'POST',
                body,
                credentials: 'include'
            }),
            invalidatesTags: ['Account'],
        }),

        login: builder.mutation({
            query: ({ username, password }) => {
                const body = new FormData();
                body.append('username', username);
                body.append('password', password);
                return {
                    url: `/token`,
                    method: 'POST',
                    body,
                    credentials: 'include',
                };
            },
            invalidatesTags: ['Account'],
        }),
    }),
});

export const {
    useGetAccountQuery,
    useLogoutMutation,
    useLoginMutation,
    useSignupMutation,
    useGetGameByIdQuery,
    useGetallGamesQuery,
    useGetPlatformsQuery,
    useGetGenreQuery,
    useGetDeveloperQuery,
    useGetPublisherQuery,

    useGetFavoritesQuery,
    useCreateFavoriteMutation,
    useDeleteFavoriteMutation,
} = gameApi;
