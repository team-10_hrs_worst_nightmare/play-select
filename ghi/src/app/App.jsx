import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import Nav from '../Components/Nav';
import Footer from '../Components/Footer';
import Home from '../Components/Home';
import Favorites from '../Components/Favorites';
import Creators from '../Components/Creators';
import Login from '../Accounts/Login';
import SignUp from '../Accounts/SignUp';
import GameList from '../Components/GameList';
import GameDetails from '../Components/GameDetails';
import GenreList from '../Components/GenreList';
import DeveloperList from '../Components/DeveloperList';
import PublisherList from '../Components/PublishersList';
import PlatformsList from '../Components/PlatformsList';
import Profile from '../Accounts/Profile';
import LandingPage from '../Components/LandingPage';
import Search from '../Components/Search';
import Layout from '../Components/Layout';
import { useGetAccountQuery } from '../Store/apiSlice';
import '../Styles/App.css';

function App() {
  const { data, isLoading } = useGetAccountQuery();

  if (isLoading) {
    return <div>Loading</div>;
  }

  return (
    <div className="container-fluid">
      <Nav />
      <Layout>
        <Routes>
          <Route path="/landingPage" element={data ? <Navigate to="/" /> : <LandingPage />} />
          <Route path="/signup" element={data ? <Navigate to="/" /> : <SignUp />} />
          <Route path="/login" element={data ? <Navigate to="/" /> : <Login />} />
          <Route path="/" element={data ? <Home /> : <Navigate to="/landingPage" />} />
          <Route path="/games" element={data ? <GameList /> : <Navigate to="/landingPage" />} />
          <Route path="/games/:game_id" element={data ? <GameDetails /> : <Navigate to="/landingPage" />} />
          <Route path="/search" element={data ? <Search /> : <Navigate to="/landingPage" />} />
          <Route path="/platform-list" element={data ? <PlatformsList /> : <Navigate to="/landingPage" />} />
          <Route path="/genre-list" element={data ? <GenreList /> : <Navigate to="/landingPage" />} />
          <Route path="/developer-list" element={data ? <DeveloperList /> : <Navigate to="/landingPage" />} />
          <Route path="/publisher-list" element={data ? <PublisherList /> : <Navigate to="/landingPage" />} />
          <Route path="/profile" element={data ? <Profile /> : <Navigate to="/landingPage" />} />
          <Route path="/favorites" element={data ? <Favorites /> : <Navigate to="/landingPage" />} />
          <Route path="/creators" element={<Creators />} />
        </Routes>
      </Layout>
      <Footer />
    </div>
  );
}

export default App;
