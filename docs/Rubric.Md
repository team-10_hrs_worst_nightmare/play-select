# How We're Graded

## Required technology and features

---
For the Module 3 project, you must comply with the following requirements to get full marks:

You must use FastAPI for your Web server backend. That means no Django.
You must have a log in/log out feature. When someone logs in, the person should have more functionality than when they're not logged in using the jwtdown-fastapi authentication module.
You must have a database, either MongoDB or PostgreSQL. You can make that decision when you get into it.
You must use React to build a Single-Page Application (SPA).
You must deploy the frontend and backend of your application.
Features grade (55%)

 Each person will receive an individual feature grade, based on their own contributions to the project, which is broken into 3 components:

---

## Backend work (20%)

You must implement a FastAPI endpoint that performs a critical purpose. The endpoint should:

- [X] have a RESTful URL
- [X] make use of pydantic data validation
- [X] handle errors appropriately
- [X] get or put data to/from a data store or perform other business logic

---

## Frontend work (20%)

You must implement a React component that:

- [X] utilizes one or more hooks or RTK method
- [X] a click-handler or use of useEffect
- [ ] gets/updates remote data using appropriate method

---

## Unit tests (15%)

You must implement at least one unit test for your application that:

- [ ] mocks input and output data
- [ ] handles overrides/dependency-injection properly
- [ ] tests some meaningful logic

---

## Extra credit (5%)

Note: don't bet your grade on these 5 points. You can get an extra 5 points if you go above and beyond the basics above; the following includes work in any of these areas:

- [ ] web sockets
- [x] integration with a 3rd party API
- [X] use of Redux/RTK
- [ ] above average CSS work

---

## Peer review (30%)

Your team will review you, too. If you slack off or don't do anything, then you will fail this project, friend.

Your teammates will score you on the following criteria:

- Confidence: Teammate's belief that you can accomplish a task that you commit to

- Coordination: Your ability to set, plan, monitor, and complete tasks within a specific time frame

- Control: Your ability to diagnose and solve your own bugs

- Composure: Your ability to remain calm and have a positive attitude when things go wrong

- Commitment: Your ability to stay on task, ignore distractions, act on feedback, and recover from setbacks

- Collaboration: Your ability to work with others to achieve common goals

They will then be asked, Would you be willing to work with this person again? The answer to this question worth a large portion of the peer review.

## Process and quality grade (15%)

### Code cleanliness: 5%

If your code formatting and syntax is all over the place, you'll get points deducted. Hiring managers will be reviewing the code in this project, so let's make sure it's easy to read. Use black on the Python files. Use "Format Document" in Visual Studio Code on your JavaScript files.

### Things to look out for: 👀

Dead code. Get rid of code that is unused, unreachable, commented out, etc.
Code complexity should be reasonable. If your files are indented into a single massive sideways pyramid, it's time to refactor.

### Documentation: 5%

The README.md file for the project and associated files in your docs directory should accurately represent what the application is, what it does, how it's built, what the future of the application would be, have correct spelling and grammar, and contain links to the deployed application in its staging environment. This is also something that hiring managers will look at, so do it right for them (and for this grade). Here's an example of a very nice README.md

### GitLab usage: 5%

The points here are for appropriate use of Git and GitLab for:

- [ ] creating GitLab issues for the work to be done

#### Each issue must have

- [ ] A merge request with an approval from at least one non-authoring team member BEFORE merging into the main branch. We encourage others on the team to comment.
- [ ] At least one test deployed in continuous integration per merge-request when backend endpoints are created or edited.
- [ ] use of feature branches for development work

- [ ] use of merge requests for adding contributions to the team code base

- [ ] tracking of tasks, per the team-agreed policy
