# API Documentation

* Special thanks to RAWG for access to their free API key allowing us to utilize their video game database.
* <https://rawg.io/apidocs>

[![Alt text](/ghi/src/images/RAWG-logo.png)](https://rawg.io/apidocs)

## Endpoints for Games

## List games

* Path: /games
* Method: GET

* Headers:
  * Authorization: Bearer Token

* Response: A list of video games
* Response Shape (JSON):

    ```json
    {
      "games": [
        {
          "id": 0,
          "slug": "string",
          "name": "string",
          "released": "string",
          "tba": false,
          "background_image": "string",
          "rating": 0,
          "rating_top": 0,
          "ratings": {"dict"},
          "ratings_count": 0,
          "reviews_text_count": "string",
          "added": 0,
          "added_by_status":  {
            "yet": 1072,
            "owned": 10760,
            "beaten": 4463,
            "toplay": 738,
            "dropped": 857,
            "playing": 842
          },
          "metacritic": 0,
          "playtime": 0,
          "suggestions_count": 0,
          "updated": "string",
          "esrb_rating": {"dict"},
          "platforms": [{"dict"}]
        }
      ]
    }
    ```

## Get game details

* Path: /games/{game_id}
* Method: GET

* Headers:
  * Authorization: Bearer Token

* Response: A list of video games
* Response Shape (JSON):

    ```json
        {
          "id": 0,
          "slug": "string",
          "name": "string",
          "released": "string",
          "tba": false,
          "background_image": "string",
          "rating": 0,
          "rating_top": 0,
          "ratings": {"dict"},
          "ratings_count": 0,
          "reviews_text_count": "string",
          "added": 0,
          "added_by_status": {
            "yet": 1072,
            "owned": 10760,
            "beaten": 4463,
            "toplay": 738,
            "dropped": 857,
            "playing": 842
          },
          "metacritic": 0,
          "playtime": 0,
          "suggestions_count": 0,
          "updated": "string",
          "esrb_rating": {"dict"},
          "platforms": [{"dict"}]
        }
    ```

## Get a list of publishers

* Method: GET
* Path: /publishers
* Headers:
  * Authorization: Bearer Token
* Response Shape (JSON):

    ```json
    {
      "publishers": [
        {
          "id": 0,
          "name": "string"
        }
      ]
    }
    ```

## Get a list of platforms

* Method: GET
* Path: /platforms
* Headers:
  * Authorization: Bearer Token
* Response Shape (JSON):

    ```json
    {
      "platforms": [
        {
          "id": 0,
          "name": "string",
          "slug": "string",
          "games_count": 0,
          "image_background": "string",
          "image": "string",
          "year_start": 0,
          "year_end": 0
        }
      ]
    }
    ```

## Get a list of genres

* Method: GET
* Path: /genres
* Headers:
  * Authorization: Bearer Token
* Response Shape (JSON):

    ```json
    {
      "genres":
      [
        {
          "id": 0,
          "name": "string",
          "slug": "string",
          "games_count": 0,
          "image_background": "string"
        }
      ]
    }
    ```

## Get a list of developers

* Method: GET
* Path: /developers
* Headers:
  * Authorization: Bearer Token
* Response Shape (JSON):

    ```json
    {
      "developers":
      [
        {
          "id": 0,
          "name": "string"
        }
      ]
    }
    ```

## Endpoints for Accounts

## Login

* Method: POST
* Path: /token
* Response Shape (JSON):

  ```json
  {
    "access_token": "string",
    "token_type": "Bearer"
  }
  ```

## Logout

* Method: DELETE
* Path: /token

* Headers:
  * Authorization: Bearer Token

* Response Shape (JSON):

  ```json
  {
    "success": true
  }
  ```

## Create account

* Method: POST
* Path: /api/accounts

* Request Shape (JSON):

  ```json
  {
    "email": "string",
    "password": "string",
    "full_name": "string"
  }
  ```

* Response Shape (JSON):

  ```json
  {
    "email": "string",
    "password": "string",
    "full_name": "string"
  }
  ```

## Get token

* Method: GET
* Path: /token

* Headers:
  * Authorization: Bearer Token

* Response Shape (JSON):

  ```json
  {
    "access_token": "string",
    "token_type": "Bearer",
    "account": {
      "id": "string",
      "email": "string",
      "password": "string",
      "full_name": "string"
    }
  }
  ```

## Favorites

## Create favorite game for account

* Method: POST
* Path: /api/favorites
* Headers:
  * Authorization: Bearer Token
* Request Shape (JSON):

  ```json
  {
    "game_id": "string"
  }
  ```

* Response Shape (JSON):

  ```json
  {
    "id": "string",
    "account_id": "string",
    "game_id": "string"
  }
  ```

## List favorites for account

* Method: GET
* Path: api/favorites/mine

* Headers:
  * Authorization: Bearer Token

* Response Shape (JSON):

  ```json
    {
    "favorites":[
      {
        "id": "string",
        "account_id": "string",
        "game_id": "string"
      }
    ]
  }
  ```

## Delete a favorite

* Method: DELETE
* Path: api/favorites/{favorite_id}

* Headers:
  * Authorization: Bearer Token

* Response Shape (JSON):

  ```json
  {
    "deleted": true
  }
  ```

## Integrations

The application needs to get the following kinds of data from third-party sources:

* Video game data including data about individual video games, game publishers, developers, video game platforms, and genres from the RAWG video game API
