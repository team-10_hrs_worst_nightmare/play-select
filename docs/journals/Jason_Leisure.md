# Jason Leisure

## Day 1- 06/26/2023

* Developed backend authentication and configured router ports in collaboration with the team

## Day 2- 06/27/2023

* Designed and implemented a chat room

## Day 3- 06/28/2023
* Made progress on the development of the Developer Router component.

## Day 4- 06/29/2023

* Deployed the Developer Routers page and Developer Queries page. Added additional features to the chat room and updated the corresponding README file.

## Day 5- 06/30/2023

* Worked on creating a project logo and implementing a video player.

## Day 6- 07/10/2023

* Completed the implementation of index.js and finalized the project logo and video player.

## Day 7- 07/11/2023

* Finalized the index.js file and made progress on the chat room component.

## Day 8- 07/12/2023

* Continued working on the chat room functionality.

## Day 9- 07/13/2023
* Implemented a game recommendation function for our project and conducted research on axios, websockets, and other areas to enhance our project. Developed carousel to hold recommended game for project.

## Day 10- 07/14/2023

* Worked on the comment section feature and initiated the development of a websocket-based chat.

## Day 11- 07/17/2023

* Completed the games.jsx page and collaborated with Brandon to finalize the nav.jsx component. Implemented the websocket chat feature for the project and made progress on the project's CSS.

## Day 12- 07/18/2023

* Working on front end with css. Trying to get it where all our forms, search bars everything appear like the project demands. Will continue working on it tomorrow. Also worked on the Footer compenent and finished. Spent till 2:22am working on css done for tonight its 7/19.

## Day 13- 07/19/2023

* Working on css changes and footer.jsx finished those for now. Fixing Profile.jsx and finished. Also fixed the Favorites.jsx. Fixed the gamescard.jsx.

## Day 14- 07/20/2023

* Worked on profile.jsx and gamescard and finished them.  Implimented allowed user to upload a image and save it to their profile with the option of uploading a new picture again also worked more on css and front end.

## Day 15- 07/24/2023

* Worked on PlatformsList, PublishersList, DevelopersList, DeveloperCard, Home, Profile. Worked more on css and front end. Converted css so it wouldn't mess up with scss file.

## Day 16- 07/25/2023

* Worked more on front end, css, test publishers, home
