# Brandon Ramos

## Journals- Mod 3Project

## MOD3Project

## Day 1- 06/22/2023

* Created excalidraw diagram and wireframe for project.created shared link for team to help, ended up not using team gave me some baseline ideas and helped spell check once completed.
  * Created Diagram for both project and Extras
  * wireframe
    * Created login page
    * Created sign up page
    * Created main page
    * Created profile page
    * Created Recommendation list page
    * Created game page
    * Created game detail page
  * Wireframe Extras
    * Created My collection
    * Created game detail 2 page with char function
    * Created Users page
* Created wireframe png, allowing to separate pages.
* Submitted them to the Team Channel for review.
* myron and Valerie helped with ideas
* Jason worked on  his (strechgoals)s not sure what else he did to contribute

## Day 2- 06/26/2023

* Created gitLab Group and invited all members.
* Created  cloned and forked the repository
* Created a branch for myself and other team  members
* started the project.
  * Created  all Necessary files for docker,  mongo and mongo express.
  * personal created and coded
  * docker image
  * docker container
  * README.md
    * created and formatted all text in Real me including:
      * created by
      * design
      * Play Select APP in Action
      * Project layout
      * Set Up
        * Clone the Repo
        * Docker
        * Database
        * Front End
        * Stretch Goals
  * Docker-compose.ymal
  * .env
     *Accounts Directory
    * Routers Directory
       *auth.py
    * queries Directory
       *accounts.py
    * client.py
     *Dockerfile.dev
    * main.py
    * models.py
  * Created Front end Ideas Directory
  * in docs directory
     *created api directory's
    * created API_design.md
    * created images directory
      * added all diagram and wireframe.png’s
    * journals directory
      * created a .md file for all team members
      * edited partial of my journal.md
    * created  and filled in Wireframediragams.md
    * created  and filled in stretchgoals.md
* Team watched as I trouble shooted and coded above.
* Valerie completed code for Accounts.py in routers directory
* myron worked on getting his docker set up
* Jason worked on  his (strechgoals)s not sure what else he did to contribute
* Myron and Valerie followed along  in  live share

## Day 3- 06/27/2023

* addressed all issues with Auth and accounts and got it working before push to main.
* Troubled shooted with Siers, and team to make sure all changes to account worked same with Auth.
* team watched as I troubled shooted and coded
* team worked on getting there code to work
* Jason worked on  his (strechgoals)s not sure what else he did to contribute
* myron and Valerie worked on making sure there code worked
* Myron and Valerie followed along  in  live share

## Day 4- 06/28/2023

* Designated team split of work
  * Brandon- Games and Genres
  * Jason - developers
  * Myron- publishers
  * Valerie- Platforms

* Created  all files in Queries and Routers directory
  * so all team members need to do is put in the code
  * created bare bone Main.py, clients.py,
  * created all models in models.py
* started on my section coding  Games and Genres API endpoints
* helped Jason convert all his (stretch Goals txt into .md Files)
* Valerie helped with working on her code
* myron helped by working on his code.
* Jason worked on more strechgoals, not sure what else he did to contribute .
* Myron and Valerie followed along  in  live share

## Day 5- 06/29/2023

* Competed my code section
  * game-queries.py
  * routers/game.py
* Completed  models.py, Main.py, clients.py.
* created and completed
  * migrations directory
  * dockerfile
  * dockerfile.dev
  * keys.py( integrated it to .gitnore)
* Helped team and showed them how to create and push branches for git
* Compiled all of teams code
  * edited code to get it to work with the API
  * formatted and edited file names
  * formatted and edited all team code to be concise and complete
  * made sure all API end points were complete
  * trouble shooted all errors.
* created Rubric.md file
* once Jason completed his section after working on stretch goals all week I integrated his code.
* I compiled and integrated all code after  everyone was complete, I sent in a push and merge to main.
* Myron and Valerie followed along  in  live share
* Jason worked on more strechgoals again .
* I finished all the work and had everything running by my last push.

## Day 6 -07/10/23

* Started on front end
* created a logo
* created and set up organization of the front end files
* added several files such as
  * index.css
  * Custom.css
  * nav.js
  * mainpage.js
  * added controller , play select and RAWGlogo
  * created login and sign up

## Day 7- 07/11/23

* worked on errors in the login and sign up form
* worked on making it work to the
* created api slice code
* created search slice code
* created store code
* edited index.css
* created code app.jsx
* edited main page
* created/edit nav
* creates/edit index.

## Day 8 07/12/23

* worked all day on getting the backend to communicate to the front end, worked on quieres and  routers in the back end

## Day 9 07/13/23

* finally resolved the back end issues and workers on creating and deleting files. Worked on organization of the front end files. Created folders.
* Spent hour on walking Jason though how to peppery set up his branch on git.
* had to go back and edit backend code to make work again, worked on names and other issues. Regarding accounts and login and sign up features,
* thought the day and at end of day I worked on getting all  group members branches pushed and merged

## Day 10 7/14/23

* updated docker read me,
* merged Valerie's branch to main.
* updated the git lab-cl.ymal file , git more, key pages, and other merge request from group.
* worked with Jason on how to properly set up Docker on his computer.
* worked with Jason on giving him tasks to work on, created the files and told him to create the code. (Did not get the code)
* worked on completing all the code, and working with myron on debugging the code.
* updated/ created/ edited
  * api/ queries
  * accounts.py
  * Ghi/src/accounts
    * Login.jsx
  * components
    * ErrorPage.jsx
    * Favorites.jsx
    * Footer.jsx
    * GameCard.jsx
    * Gamedetail.jsx
    * GameList.jsx
    * Home.jsx
    * Nav.jsx
    * Platformlist.jsx
    * Games.jsx
  * store
    * apiSlice
  * Styles
    * index.css
  * app
    * app.jsx
  * images
    * controller
  * index.js
  * dockerfile.dev
  * package-lock.json
  * package.jason

## Day 11 07/17/23

* worked on getting all the remainder code files created
* worked on making the code for files that were created
* worked on main branch to help with workflow and margin with myron and Valerie.
* worked with he nav bar and all the card files to get them intrigued with the list.
* worked on renaming files and organization for  code that got moved or adjusted during merging.
* worked with Jason on telling him to work on some files but those files never got  done . So I had to create and work with myron to make those files. Such as editing the nav bar to his liking as well as some css.

## Day 12 07/18/23

* worked all day attempting to work with he back end and front end to get the the code that Jason that broke the code.
* myron and I worked on fixing all the code to get it to work properly again
* Valerie worked on the test and she explained to us all about how to create the unit tests.
* finally got the code working at end of day and able to move on.
* worked on attempting t0 get carousel to work for Jason,.
* created api backend to get from api screen shots of games. Was unable to get the code to work and moved on to  fixing the backend to connect with mongo db.
* worked on commits and merges with each team member at the end of the day so that we had up to date code. Worked on queries and routers and  for mongo.
* created organization to

## Day 13  07/19/23

* Worked on catching up on my journal
* explained to Jason how to use git branch, push, pull, merge.
* Worked with Jason on making sure  his branch was up to date with main.  checked out to his branch and worked though 129 merge conflicts with him
* worked on getting the game cards to work one the front end material.

## Day 14 07/20/2023

* finished up the cards and list files as well as updated and fixed any merge changes to the main branch.
* pushed files I worked on though out the day and assisted with other team members in small changes to my end code..
* completed an end of day code clean up after class so the  team would have a clean branch to pull from the next day,
* that included docker readme and adding empty test files.
* handled some issues on the main branch due to code breaks and was able to update NPM and other packages.
* back end code broke and was able to Fix code and make more changes in the backend.

## Day 15 07/24/2023

* worked on creator page and getting images set up so that team can have there page available to view.  Worked on routes and other index and app issues on the front end.
* created a basic sass and nav bar and footer with links to  api and git lab.
* worked on login and sign up form, as well some landing pages that were causing issues

## Day 16 07/25/2023

* was able to work on front end auth and making private routes and other merge issues. Worked on sever files related to front end auth and worked on the accounts folder and profile page.
* made changes with fast API we had two fast api folders and I worked on making them into one. And functional of the back end code now that it was merged into one app.

## Day 17 07/26/2023

* Worked on docker and with Jason on how to set up docker.
* Walked tough the steps to create a docker file and docker dev files, as well as ymal
* worked on the process on  how to build containers and set up jason's docker via share screen.

## Day 18 07/27/2023

* Was able to finish and complete the favorites button and was able to get it to work until merge changes then worked on getting all broken files back to working state.
* able to get the fav button to work and worked on fixing the slice and getting redux running in the front end correctly.
* was able to fix some of the code and work though the issues..
* assigned tasks for the day and attempted to make sure that there was not any issues and the code was working at the end of the day.
* worked on the f favorites and favorites id attempting to get them to render and the team to access files.  Worked on making all front end code organized and properly running created folders and css. Was able to work and get the redux to run had to swap the files as I was still working on getting the login function to work as well as other functions of the website. Was start of issues with the favorites button with all the incoming changes to the branch. worked with myron to get favorites set up.
* worked with val on getting the test set ip as she instructed me to complete the new task.  Had sever issues with the favorites nav bar and many other  code  was unable to have the sight working by the end of the day submits. I was able to work over night until class the next day to have the code ready for  the team to be able to login and sign up there wee many issues was able to crate and  log out of the site. Decided that there was no use for the screen shots and other files that were tossed out. I was able tole to work on changes and updates to the css and fixing broken code. Was unable to get the favorites button working by class time so I woked with Myron the next day to finish the button. Was able to  condense files and used code that was slowing down the application. Was able to get the webpage up and running by the start of class.

## Day 19/28/2023

Final day I was able to get all the code working and usable fly start of class so that we could all have correct working updated code on the main branch. There were some issues that such as the open issues the sign uip form not working, test and other things I was unable too commit in the moving  was able to work on some code to get the profile page set up as well as the other files and made the sight more modern , was able too generate the favorites card and all other cards I was able to get the code working decided the favorites page.
Finally was able to work with myron and Valerie to complete all missing files that were left in the file and fit some final code clean up and push , was able too get even thing working and running the profile page was not fully complete as well as the other css . Was able to do a walk though with the team and finish up my journals  and other loose end files . Submitted project and then was able to finish journals and make one last push.
