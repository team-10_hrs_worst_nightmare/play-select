from typing import List

from fastapi import APIRouter, Depends
from models import Publisher
from queries.publisher_queries import PublisherQueries

router = APIRouter()


@router.get("/publishers", response_model=List[Publisher])
def get_publishers_endpoint(
    repo: PublisherQueries = Depends()
):
    platforms_list = repo.get_publishers()
    return platforms_list
