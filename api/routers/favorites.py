from fastapi import APIRouter, Depends
from models import FavoritesList, FavoriteIn, FavoriteOut
from queries.favorites import FavoriteQueries
from authenticator import authenticator


router = APIRouter()


@router.get('/favorites/mine', response_model=FavoritesList)
def list_favorites_for_current_account(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: FavoriteQueries = Depends(),
):
    favorites = queries.list_all_for_account(account_id=account_data['id'])
    return {'favorites': favorites}


@router.post('/favorites', response_model=FavoriteOut)
def create_favorite(
    favorite_in: FavoriteIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: FavoriteQueries = Depends()
):
    favorite = queries.create(
        favorite_in=favorite_in, account_id=account_data['id'])
    return favorite


@router.delete('/favorites/{favorite_id}')
def delete_favorite(
    favorite_id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: FavoriteQueries = Depends()
):
    queries.delete(
        favorite_id=favorite_id,
        account_id=account_data['id']
    )
    return {'success': True}
