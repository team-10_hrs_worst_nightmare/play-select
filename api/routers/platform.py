from typing import List

from fastapi import APIRouter, Depends
from models import Platform
from queries.platform_queries import PlatformQueries

router = APIRouter()


@router.get("/platforms", response_model=List[Platform])
def get_platforms_endpoint(
    repo: PlatformQueries = Depends()
):
    platforms_list = repo.get_platforms()
    return platforms_list
