from fastapi import APIRouter, Depends
from models import Developer
from typing import List
from queries.developer_queries import DeveloperQueries


router = APIRouter()


@router.get("/developers", response_model=List[Developer])
def get_developers(repo: DeveloperQueries = Depends()):
    developers_list = repo.get_developers()
    return developers_list
