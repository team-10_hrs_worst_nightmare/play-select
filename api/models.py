from bson.objectid import ObjectId
from pydantic import BaseModel
from typing import List, Optional


class ESRBRating(BaseModel):
    id: int
    slug: str
    name: str


class Platform(BaseModel):
    id: Optional[int]
    name: str
    slug: str
    games_count: int
    image_background: str
    image: str
    year_start: int
    year_end: int


class PlatformList(BaseModel):
    count: int
    next: str
    previous: str
    results: List[Platform]


class Platform(BaseModel):
    id: int
    name: str
    slug: str
    games_count: int
    image_background: str
    image: str
    year_start: int
    year_end: int


class Game(BaseModel):
    id: int
    name: str
    developer: str
    Genre: str
    Platforms: List[Platform]
    Publisher: str


class GameIn(BaseModel):
    id: int
    game_name: str
    developer: str
    genre: str
    platforms: List[Platform]
    publisher: str


class GameOut(BaseModel):
    id: str
    game_name: str
    developer: str
    genre: str
    platforms: List[Platform]
    publisher: str


class GameList(BaseModel):
    id: int
    game_name: str
    developer: str
    genre: str
    platforms: List[Platform]
    publisher: str


class Developer(BaseModel):
    id: int
    name: str
    games_count: int


class Genre(BaseModel):
    id: int
    name: str
    slug: str
    games_count: int
    image_background: str


class GenreList(BaseModel):
    count: int
    next: Optional[str]
    previous: Optional[str]
    results: List[Genre]


class Publisher(BaseModel):
    id: int
    name: str
    slug: str
    games_count: int
    image_background: str


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: ObjectId | str) -> ObjectId:
        try:
            return cls(value)
        except (TypeError, ValueError):
            raise ValueError(f"Invalid ObjectId: {value}")


class FavoriteIn(BaseModel):
    game_id: str
    game_name: str


class FavoriteOut(BaseModel):
    id: str
    account_id: str
    game_id: str
    game_name: str


class FavoritesList(BaseModel):
    favorites: List[FavoriteOut | None]


class AccountIn(BaseModel):
    email: str
    password: str
    first_name: str
    last_name: str
    username: str


class Account(AccountIn):
    id: PydanticObjectId
    roles: List[str]


class AccountOut(BaseModel):
    id: str
    email: str
    first_name: str
    last_name: str
    username: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str
