from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from authenticator import authenticator

from routers import (
    games, genre, platform, developer, publisher,
    accounts, favorites
)


app = FastAPI()

origins = [
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),

]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(games.router, tags=["Games"])
app.include_router(developer.router, tags=["Developer"])
app.include_router(platform.router, tags=["Platform"])
app.include_router(publisher.router, tags=["Publisher"])
app.include_router(genre.router, tags=["Genre"])
app.include_router(authenticator.router, tags=["accounts"])
app.include_router(accounts.router, tags=["Token"])
app.include_router(favorites.router, tags=["Favorites"])


app.get("/")


def home():
    return True
