from fastapi.testclient import TestClient
from main import app
from queries.favorites import FavoriteQueries
from models import FavoriteIn
from authenticator import authenticator
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "1234", "username": "fake"}


class FakeFavoriteQueries:
    def create(self, favorite_in: FavoriteIn, account_id: str):
        favorite = favorite_in.dict()
        favorite["id"] = "fake_id"
        favorite["account_id"] = account_id
        favorite["game_name"] = favorite_in.game_name
        return favorite

    def list_all_for_account(self, account_id: str):
        return [
            {
                "game_id": "4567",
                "id": "gdshj8adjfdsjgafkll2",
                "account_id": account_id,
                "game_name": "SomeGameName",
            }
        ]

    def delete(self, favorite_id: str, account_id: str):
        return True


def test_create_favorite():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    favorite_in = {"game_id": "6789", "game_name": "SomeGameName"}

    res = client.post("/favorites", json=favorite_in)
    data = res.json()

    assert data == {
        "id": "fake_id",
        "account_id": "1234",
        "game_id": "6789",
        "game_name": "SomeGameName",
    }
    assert res.status_code == 200


def test_list_favorites():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.get("/favorites/mine")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "favorites": [
            {
                "game_id": "4567",
                "id": "gdshj8adjfdsjgafkll2",
                "account_id": "1234",
                "game_name": "SomeGameName",
            }
        ]
    }


def test_delete_favorite():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.delete("/favorites/1233456")
    data = res.json()

    assert res.status_code == 200
    assert {"success": True} == data
