from fastapi.testclient import TestClient
from main import app
from queries.developer_queries import DeveloperQueries


client = TestClient(app)


class FakeDeveloperQueries:
    def get_developers(self):
        return [
            {
                "id": 1,
                "name": "Ubisoft",
                "games_count": 100,
            }
        ]


def test_get_developers_list():
    app.dependency_overrides[DeveloperQueries] = FakeDeveloperQueries

    res = client.get("/developers")
    data = res.json()

    assert res.status_code == 200
    assert data == [
        {
            "id": 1,
            "name": "Ubisoft",
            "games_count": 100,
        }
    ]
