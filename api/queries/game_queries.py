import os
import pymongo
from typing import List
import httpx
from fastapi import HTTPException
from models import Game, Platform

MONGO_URL = os.environ["DATABASE_URL"]
client = pymongo.MongoClient(MONGO_URL)

RAWG_API_KEY = os.environ["RAWG_API_KEY"]


class GameQueries:
    COLLECTION = "Game"
    DB_NAME = "PlaySelect"
    collection = client[DB_NAME][COLLECTION]


async def fetch_games() -> List[Game]:
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                f"https://api.rawg.io/api/games?key={RAWG_API_KEY}"
            )
            response.raise_for_status()
            data = response.json()

        games = []
        queries = GameQueries()

        for game in data["results"]:
            try:
                ratings = game.get("ratings", {})
                if not isinstance(ratings, dict):
                    ratings = {}

                platforms = []
                for platform in game.get("platforms", []):
                    try:
                        image = platform["platform"]["image"] or ""
                        year_start = platform["platform"]["year_start"]
                        if year_start is not None:
                            try:
                                year_start = int(year_start)
                            except (ValueError, TypeError):
                                year_start = 0
                        else:
                            year_start = 0

                        year_end = platform["platform"]["year_end"]
                        if year_end is not None:
                            try:
                                year_end = int(year_end)
                            except (ValueError, TypeError):
                                year_end = 0
                        else:
                            year_end = 0

                        platform_obj = Platform(
                            id=platform["platform"]["id"],
                            name=platform["platform"]["name"],
                            slug=platform["platform"]["slug"],
                            games_count=platform["platform"]["games_count"],
                            image_background=platform["platform"][
                                "image_background"
                            ],
                            image=image,
                            year_start=year_start,
                            year_end=year_end,
                        )
                        platforms.append(platform_obj)
                    except KeyError as e:
                        print(f"Missing field in platform data: {e}")
                        print(f"Platform data: {platform}")
                        raise

                game_obj = Game(
                    id=game["id"],
                    name=game["name"],
                    developer="",
                    Genre="",
                    Platforms=platforms,
                    Publisher=""
                )

                queries.collection.insert_one(game_obj.dict())

                games.append(game_obj)

            except KeyError as e:
                print(f"Missing field in game data: {e}")
                print(f"Game data: {game}")
                raise

        return games

    except httpx.RequestError as e:
        raise HTTPException(status_code=500, detail=str(e))


async def fetch_game_by_id(game_id: str) -> Game:
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(
                f"https://api.rawg.io/api/games/{game_id}?key={RAWG_API_KEY}"
            )
            response.raise_for_status()
            data = response.json()

            ratings = data.get("ratings", {})
            if not isinstance(ratings, dict):
                ratings = {}

            metacritic = data.get("metacritic", 0)
            if metacritic is None:
                metacritic = 0

            platforms = data.get("platforms", [])

            platform_objs = []
            for platform in platforms:
                platform_data = platform.get("platform", {})
                image = platform_data.get("image")
                if image is None:
                    image = ""

                year_start = platform_data.get("year_start", 0)
                if year_start is not None:
                    try:
                        year_start = int(year_start)
                    except (ValueError, TypeError):
                        year_start = 0
                else:
                    year_start = 0

                year_end = platform_data.get("year_end", 0)
                if year_end is not None:
                    try:
                        year_end = int(year_end)
                    except (ValueError, TypeError):
                        year_end = 0
                else:
                    year_end = 0

                platform_obj = Platform(
                    id=platform_data["id"],
                    name=platform_data["name"],
                    slug=platform_data["slug"],
                    games_count=platform_data["games_count"],
                    image_background=platform_data["image_background"],
                    image=image,
                    year_start=year_start,
                    year_end=year_end,
                )
                platform_objs.append(platform_obj)

            game = Game(
                id=data["id"],
                name=data["name"],
                developer="",
                Genre="",
                Platforms=platform_objs,
                Publisher=""
            )

            return game

    except httpx.RequestError as e:
        raise HTTPException(status_code=500, detail=str(e))
