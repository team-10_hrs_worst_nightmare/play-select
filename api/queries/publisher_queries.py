import os
from typing import List

import pymongo
import requests
from fastapi import HTTPException
from models import Publisher
from queries.client import Queries

MONGO_URL = os.environ["DATABASE_URL"]
client = pymongo.MongoClient(MONGO_URL)

RAWG_API_KEY = os.environ["RAWG_API_KEY"]


class PublisherQueries(Queries):
    COLLECTION = "Publisher"
    DB_NAME = "PlaySelect"

    def get_publishers(self) -> List[Publisher]:
        try:
            response = requests.get(
                f"https://api.rawg.io/api/publishers?token&key={RAWG_API_KEY}"
            )
            response.raise_for_status()
            data = response.json()

            publishers = []
            collection = self.collection

            for publisher_data in data["results"]:
                publisher = Publisher(
                    id=publisher_data["id"],
                    name=publisher_data["name"],
                    slug=publisher_data["slug"],
                    games_count=publisher_data["games_count"],
                    image_background=publisher_data.get(
                        "image_background", ""),
                )

                collection.insert_one(publisher.dict())
                publishers.append(publisher)

            return publishers

        except requests.exceptions.RequestException as e:
            raise HTTPException(status_code=500, detail=str(e))
