from .client import Queries
from models import AccountOutWithPassword, AccountIn, AccountOut


class DuplicateAccountError(ValueError):
    pass


class AccountQueries(Queries):
    DB_NAME = "accounts"
    COLLECTION = "accounts"

    def get_account(self, username: str) -> AccountOutWithPassword:
        account = self.collection.find_one({"username": username})
        if account is None:
            return None
        account["id"] = str(account["_id"])
        return AccountOutWithPassword(**account)

    def create(self, info: AccountIn, hashed_password: str) -> AccountOut:
        account = info.dict()
        account["hashed_password"] = hashed_password
        response = self.collection.insert_one(account)
        account["id"] = str(response.inserted_id)
        return AccountOutWithPassword(**account)

    def get_all_accounts(self) -> list[AccountOut]:
        accounts = self.collection.find()
        account_list = []
        for account in accounts:
            account["id"] = str(account["_id"])
            account_list.append(AccountOut(**account))
        return account_list
