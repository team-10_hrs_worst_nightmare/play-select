import os
from typing import List

import pymongo
import requests
from fastapi import HTTPException
from models import Developer
from queries.client import Queries

MONGO_URL = os.environ["DATABASE_URL"]
client = pymongo.MongoClient(MONGO_URL)

RAWG_API_KEY = os.environ["RAWG_API_KEY"]


class DevelopersQueries(Queries):
    COLLECTION = "Developers"
    DB_NAME = "PlaySelect"


class DeveloperQueries:
    @staticmethod
    def get_developers() -> List[Developer]:
        try:
            response = requests.get(
                f"https://api.rawg.io/api/developers?token&key={RAWG_API_KEY}"
            )
            response.raise_for_status()
            data = response.json()
            developers = []
            for developer in data["results"]:
                developers.append(Developer(
                    id=developer["id"],
                    name=developer["name"],
                    games_count=developer["games_count"]
                ))

            db = client[DevelopersQueries.DB_NAME]
            collection = db[DevelopersQueries.COLLECTION]
            developers_data = [developer.dict() for developer in developers]
            collection.insert_many(developers_data)

            return developers

        except requests.exceptions.RequestException as e:
            raise HTTPException(status_code=500, detail=str(e))
