import os
from typing import List

import pymongo
import requests
from fastapi import HTTPException
from models import Genre, GenreList
from queries.client import Queries
from pymongo.cursor import Cursor


MONGO_URL = os.environ["DATABASE_URL"]
client = pymongo.MongoClient(MONGO_URL)

RAWG_API_KEY = os.environ["RAWG_API_KEY"]


class GenreQueries(Queries):
    COLLECTION = "Genre"
    DB_NAME = "PlaySelect"

    def get_genres(self) -> List[Genre]:
        try:
            url = f"https://api.rawg.io/api/genres?key={RAWG_API_KEY}"
            response = requests.get(url)
            response.raise_for_status()
            data = response.json()

            genres = []
            collection = self.collection

            for genre_data in data["results"]:
                genre = Genre(
                    id=genre_data["id"],
                    name=genre_data["name"],
                    slug=genre_data["slug"],
                    games_count=genre_data["games_count"],
                    image_background=genre_data.get("image_background", ""),
                )

                collection.insert_one(genre.dict())
                genres.append(genre)

            return genres

        except requests.exceptions.RequestException as e:
            raise HTTPException(status_code=500, detail=str(e))

    def get_genre_list(self) -> GenreList:
        try:
            db = client[self.DB_NAME]
            collection = db[self.COLLECTION]
            genres: Cursor = collection.find()

            genre_list = GenreList(
                count=genres.count(),
                results=[Genre(**genre) for genre in genres]
            )
            return genre_list

        except Exception as e:
            raise HTTPException(status_code=500, detail=str(e))
