import os
from typing import List

import pymongo
import requests
from fastapi import HTTPException
from models import Platform
from queries.client import Queries

MONGO_URL = os.environ["DATABASE_URL"]
client = pymongo.MongoClient(MONGO_URL)

RAWG_API_KEY = os.environ["RAWG_API_KEY"]


class PlatformQueries(Queries):
    COLLECTION = "Platform"
    DB_NAME = "PlaySelect"

    def get_platforms(self) -> List[Platform]:
        try:
            response = requests.get(
                f"https://api.rawg.io/api/platforms?token&key={RAWG_API_KEY}"
            )
            response.raise_for_status()
            data = response.json()

            platforms = []
            queries = PlatformQueries()
            collection = queries.collection

            for platform_data in data["results"]:
                games_count = platform_data.get("games_count", 0)
                image_background = platform_data.get("image_background", "")
                image = platform_data.get("image", "") or ""
                year_start = platform_data.get("year_start", 0)
                year_end = platform_data.get("year_end", 0)

                if year_start is None:
                    year_start = 0
                if year_end is None:
                    year_end = 0

                platform = Platform(
                    id=platform_data["id"],
                    name=platform_data["name"],
                    slug=platform_data["slug"],
                    games_count=games_count,
                    image_background=image_background,
                    image=image,
                    year_start=int(year_start),
                    year_end=int(year_end),
                )

                collection.insert_one(platform.dict())
                platforms.append(platform)

            return platforms

        except requests.exceptions.RequestException as e:
            raise HTTPException(status_code=500, detail=str(e))
